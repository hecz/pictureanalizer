import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by ruslan on 09.10.2014.
 */
public class UrlWorker {

    public static String getHTML(URL url) {
        String HTML = "";
        String line;
        InputStream is = null;
        try {
            is = url.openStream();  // throws an IOException
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            while ((line = br.readLine()) != null) {
                HTML = HTML.concat(line);
            }
        } catch (MalformedURLException mue) {
            mue.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            return HTML;


        }
    }

    public static URL getHttpAdress(){
        System.out.print("Enter the address of the site to count the pictures:");



        boolean KorektHttpAdress =false;

        URL url = null;
        while (!KorektHttpAdress) {

            Scanner sc = new Scanner(System.in);

            String httpAdress = sc.nextLine();

            try {
                url = new URL(httpAdress);
                KorektHttpAdress = true;
            } catch (MalformedURLException e) {
                System.err.print("Adress is inkorekt, please type again:");
            }
        }
        return url;
    }

}







