import org.htmlcleaner.XPatherException;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class Starter {

    public static void main(String[] args) throws IOException, XPathExpressionException, ParserConfigurationException, SAXException, XPatherException {

        URL url = UrlWorker.getHttpAdress();

        String textHTML = UrlWorker.getHTML(url);

        Integer count = Parser.GetImgCount(textHTML);

        System.out.print("Found "+count+" images");

    }


}
