
import org.htmlcleaner.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

/**
 * Created by ruslan on 09.10.2014.
 */
public class Parser {

    public static Integer GetImgCount(String html) throws XPathExpressionException, ParserConfigurationException, XPatherException {

        HtmlCleaner htmlCleaner = new HtmlCleaner();
        CleanerProperties props = htmlCleaner.getProperties();
        props.setAllowHtmlInsideAttributes(false);
        props.setAllowMultiWordAttributes(true);
        props.setRecognizeUnicodeChars(true);
        props.setOmitComments(true);

        TagNode root = htmlCleaner.clean(html);

        Object[] imgNodes = root.evaluateXPath("//img[@src]");

        for (Object ob:imgNodes){

            TagNode obNode = (TagNode) ob;

            System.out.println(obNode.getAttributeByName("src"));
        }

//        TagNode tagNode = new HtmlCleaner().clean(html);
//        org.w3c.dom.Document doc = new DomSerializer(
//                new CleanerProperties()).createDOM(tagNode);
//
//        XPath xpath = XPathFactory.newInstance().newXPath();
//        xpath.compile("//html");
//
//        DTMNodeList object = (DTMNodeList) xpath.evaluate("//img[@src]",
//        doc, XPathConstants.NODESET);
//
//        for (int j = 1; j <= object.getLength(); j=j+1) {
//
//            com.sun.org.apache.xerces.internal.dom.ElementImpl node = (com.sun.org.apache.xerces.internal.dom.ElementImpl) object.item(j);
//
//            System.out.println(node.toString());
//        }
//
//        return ((DTMNodeList) object).getLength();

        return imgNodes.length;
    }

}
